package uj.jwzp2019.hellospring.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import uj.jwzp2019.hellospring.model.Person;
import uj.jwzp2019.hellospring.model.Planet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
public class PeopleService {

    private final String starWarsApiUrl;
    private final RestTemplate restTemplate;
    private final PlanetService planetService;

    @Autowired
    public PeopleService(@Value("${starwars.api.url}") String starWarsApiUrl, RestTemplate restTemplate, PlanetService planetService) {
        this.starWarsApiUrl = starWarsApiUrl;
        this.restTemplate = restTemplate;
        this.planetService = planetService;
    }

    public Person getPersonById(int id) {
        Person result = getPersonByUrl(starWarsApiUrl + "/people/" + id);
        return result;
    }

    public Person getPersonByUrl(String url) {
        JsonNode jsonNodePerson = restTemplate.getForObject(url, JsonNode.class);
        Person result = JsonNodeToPersonObject(jsonNodePerson);
        return result;
    }

    private Person JsonNodeToPersonObject(JsonNode person) {
        Person result = new Person();
        Planet planet = planetService.getPlanetByUrl(person.get("homeworld").asText());
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            result = objectMapper.readValue(person.toString(), Person.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        result.setPlanet(planet);
        return result;
    }

    public List<Person> getPeopleInRangeWithEyeColor(int fromId, int toId, String color) {
        ArrayList<Person> result = new ArrayList<>();

        if (fromId > toId) {
            throw new IllegalArgumentException();
        }

        for (int i = fromId; i <= toId; i++) {
            Person tmp = getPersonById(i);
            if (tmp.getEye_color().equals(color)) {
                result.add(tmp);
            }
        }
        return result;
    }
}
