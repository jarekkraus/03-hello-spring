package uj.jwzp2019.hellospring.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import uj.jwzp2019.hellospring.model.Planet;

import java.io.IOException;

@Service
public class PlanetService {

    private final String starWarsApiUrl;
    private final RestTemplate restTemplate;

    @Autowired
    public PlanetService(@Value("${starwars.api.url}") String starWarsApiUrl, RestTemplate restTemplate) {
        this.starWarsApiUrl = starWarsApiUrl;
        this.restTemplate = restTemplate;
    }

    public Planet getPlanetById(int id) {
        Planet result = getPlanetByUrl(starWarsApiUrl + "/planets/" + id);
        return result;
    }

    public Planet getPlanetByUrl(String url) {
        JsonNode jsonNodePlanet = restTemplate.getForObject(url, JsonNode.class);
        Planet result = JsonNodeToPlanetObject(jsonNodePlanet);
        return result;
    }

    private Planet JsonNodeToPlanetObject(JsonNode planet) {
        Planet result = new Planet();

        if (planet.get("population").asText().equals("unknown")) {
            ((ObjectNode) planet).put("population", "0");
        }
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            result = objectMapper.readValue(planet.toString(), Planet.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    public Planet getSmallestPlanetInRange(int fromId, int toId) {

        if (fromId > toId) {
            throw new IllegalArgumentException();
        }

        Planet smallestPlanet = getPlanetById(fromId);
        for (int i = fromId + 1; i <= toId; i++) {
            Planet tmp = getPlanetById(i);
            if (tmp.getDiameter() < smallestPlanet.getDiameter()) {
                smallestPlanet = tmp;
            }
        }
        return smallestPlanet;
    }
}
